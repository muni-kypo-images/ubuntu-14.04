#!/bin/bash -x

# set GRUB_TIMEOUT to 0
sudo sed -i "s/^GRUB_TIMEOUT=[0-9]*/GRUB_TIMEOUT=0/" /etc/default/grub
sudo update-grub

# disable root login using password
sudo passwd -l root

# install resolvconf
apt-get -y install resolvconf

# fix #1
cat > /root/multinic.sh<< 'EOF'
#!/bin/bash
hname=$(hostname)
cat /etc/hosts | grep $hname >> /dev/null
if [ $? -ne 0 ];then
 sudo bash -c "echo '127.0.0.1 $hname' >> /etc/hosts"
fi
netfile=$(cat /etc/network/interfaces)
for interface in $(ls -1 /sys/class/net) ;do
   echo $netfile | grep $interface >> /dev/null
   if [ $? -ne 0 ];then
     sudo bash -c "echo 'auto $interface' >> /etc/network/interfaces"
     sudo bash -c "echo 'iface $interface inet dhcp' >> /etc/network/interfaces"
     sudo ifup $interface
  fi
done
EOF
echo '@reboot /root/multinic.sh' >> /var/spool/cron/crontabs/root
chmod +x /root/multinic.sh
chown root:crontab /var/spool/cron/crontabs/root
chmod 600 /var/spool/cron/crontabs/root
