#!/bin/sh -x

# apt update
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
DEBIAN_FRONTEND=noninteractive sudo apt-get update

# install ansible and linux headers for VBoxGuestAdditions
DEBIAN_FRONTEND=noninteractive sudo apt-get -y install ansible linux-generic linux-headers-generic linux-headers-server linux-image-generic linux-server

# fix networking issue
printf '\nauto eth0\niface eth0 inet dhcp\n\n' >> /etc/network/interfaces

# Reboot with the new kernel so we can build the proper guest additions
shutdown -r now
sleep 60
