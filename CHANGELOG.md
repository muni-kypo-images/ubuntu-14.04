# Changelog

## [qemu-0.2.1] - 2022-01-29
### Fixed
- Hostname is added on boot to /etc/hosts

## [qemu-0.2.0] - 2021-05-05
### Fixed
- Disabled `root` login
- [Issue #1](https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-14.04/-/issues/1) of network interfaces not being configured

## [qemu-0.1.0] - 2021-01-07
### Added
- First version


[qemu-0.1.0]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-14.04/-/tree/qemu-0.1.0
[qemu-0.2.0]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-14.04/-/tree/qemu-0.2.0
[qemu-0.2.1]: https://gitlab.ics.muni.cz/muni-kypo-images/ubuntu-14.04/-/tree/qemu-0.2.1
